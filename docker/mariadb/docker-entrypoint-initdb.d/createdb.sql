CREATE DATABASE IF NOT EXISTS `homestead` COLLATE 'utf8mb4_general_ci';
GRANT ALL ON `homestead`.* TO 'root'@'%';

CREATE DATABASE IF NOT EXISTS `homestead_test` COLLATE 'utf8mb4_general_ci';
GRANT ALL ON `homestead_test`.* TO 'root'@'%';

FLUSH PRIVILEGES ;
