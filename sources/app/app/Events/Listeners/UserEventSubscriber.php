<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace App\Events\Listeners;


use App\Vk\Token;

class UserEventSubscriber {

	/**
	 * @param  \Illuminate\Events\Dispatcher  $events
	 */
	public function subscribe($events)
	{
		$events->listen(
			'Illuminate\Auth\Events\Logout',
			'App\Events\Listeners\UserEventSubscriber@onUserLogout'
		);
	}

	public function onUserLogout($event) {
		Token::query()->delete();
	}
}