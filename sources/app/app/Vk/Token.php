<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace App\Vk;


use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Token
 * @package App\Vk
 * @property string value
 * @property string user_id
 *
 * @property User user
 */
class Token extends Model {

	protected $table = 'token';
	protected $fillable = ['value', 'user_id'];

	public function user() {
		return $this->hasOne(User::class, 'token_id', 'id');
	}
}