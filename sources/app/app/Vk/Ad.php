<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace App\Vk;


use Illuminate\Contracts\Support\Arrayable;

/**
 * Class Ad Данные объявления
 * @package App\Vk
 */
class Ad implements \JsonSerializable, Arrayable {

	protected $data;

	protected $note;

	public function __construct($data) {
		$this->data = $data;
		$this->note = Note::query()->where(['ad_id' => $data['id']])->first();
	}

	/**
	 * Specify data which should be serialized to JSON
	 * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
	 * @return mixed data which can be serialized by <b>json_encode</b>,
	 * which is a value of any type other than a resource.
	 * @since 5.4.0
	 */
	public function jsonSerialize() {
		return $this->toArray();
	}

	/**
	 * Get the instance as an array.
	 *
	 * @return array
	 */
	public function toArray() {

		return $this->data;
	}

	public function __get($name) {
		return $this->data[$name];
	}

	/**
	 * Доступные параметры объявления
	 *
	 * @return array
	 */
	public function properties() {
		return array_keys($this->data);
	}

	/**
	 * Заметка
	 *
	 * @return \Illuminate\Database\Eloquent\Model|object|static|null
	 */
	public function getNote() {
		return $this->note;
	}
}