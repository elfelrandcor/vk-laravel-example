<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace App\Vk;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Note
 * @package App\Vk
 *
 * @property integer ad_id
 * @property string text
 */
class Note extends Model {

	protected $table = 'note';
	protected $fillable = ['ad_id'];
}