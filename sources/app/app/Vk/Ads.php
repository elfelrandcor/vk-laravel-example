<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace App\Vk;


use App\Vk\Mappers\AdMapper;
use ATehnix\VkClient\Client;
use \RuntimeException;

class Ads {

	/** @var Client */
	protected $api;
	/** @var Token */
	protected $token;

	/** @var AdMapper[] */
	protected $maps = []; //todo Add mappers for all entities

	private $lastRequestTime = 0;

	public function __construct(Client $api, Token $token, array $maps) {
		$this->api = $api;
		$this->token = $token;
		$this->maps = $maps;
	}

	/**
	 * Список кабинетов
	 *
	 * @return array
	 */
	public function getAccounts(): array {
		$response = $this->doRequest('getAccounts');

		return (array)$response['response'];
	}

	/**
	 * Данные по одному кабинету
	 *
	 * @param $accountId
	 * @return mixed
	 */
	public function getAccount($accountId) {
		$list = $this->getAccounts();
		foreach ($list as $item) {
			if ($item['account_id'] == $accountId) {
				return $item;
			}
		}

		throw new RuntimeException('Account does not exists!');
	}

	/**
	 * Список кампаний для кабинета
	 *
	 * @param $accountId
	 * @return array
	 */
	public function getCampaigns($accountId): array {
		$response = $this->doRequest('getCampaigns', [
			'account_id' => $accountId,
			'include_deleted' => 0,
		]);

		return (array)$response['response'];
	}

	/**
	 * Данные по одной кампании
	 *
	 * @param $accountId
	 * @param $campaignId
	 * @return array
	 */
	public function getCampaign($accountId, $campaignId) {
		$response = $this->doRequest('getCampaigns', [
			'account_id' => $accountId,
			'include_deleted' => 0,
			'campaign_ids' => json_encode((array)$campaignId),
		]);

		return (array)$response['response'][0];
	}

	/**
	 * Список объявлений
	 *
	 * @param $accountId
	 * @param $campaignId
	 * @return array
	 */
	public function getAds($accountId, $campaignId): array {
		$response = $this->doRequest('getAds', [
			'account_id' => $accountId,
			'campaign_ids' => json_encode((array)$campaignId),
			'include_deleted' => 0,
		]);
		$mapper = $this->maps['ads'];
		$mapper->setCampaign($this->getCampaign($accountId, $campaignId));

		return array_map([$mapper, 'map'], $response['response']);
	}

	/**
	 * Удаление объявления
	 *
	 * @param $accountId
	 * @param $adId
	 * @return $this
	 */
	public function delete($accountId, $adId) {
		$this->doRequest('deleteAds', [
			'account_id' => $accountId,
			'ids' => json_encode((array)$adId),
		]);
		Note::query()->where(['ad_id' => $adId])->delete();

		return $this;
	}

	/**
	 * Выполнение запроса к ВК
	 *
	 * @param $method
	 * @param array $params
	 * @return array
	 */
	protected function doRequest($method, $params = []): array {
		$now = microtime(true);
		if ($this->lastRequestTime !== null && ($this->lastRequestTime - $now) < 500) {
			usleep(700); //try to avoid flood control
		}
		try {
			$this->api->setDefaultToken($this->token->value);
			$response = $this->api->request('ads.' . $method, $params);
		} catch (\Throwable $e) {
			if ($e->getMessage() === 'Flood control') {
				sleep(1);
				return $this->doRequest($method, $params); //todo break on N tries
			}
			//todo log
			throw new RuntimeException($e->getMessage(), 0, $e);
		}
		$this->lastRequestTime = microtime(true);

		if (!$response) {
			throw new RuntimeException('Empty response');
		}
		if (!array_key_exists('response', $response)) {
			throw new RuntimeException('Empty response');
		}

		return $response;
	}
}