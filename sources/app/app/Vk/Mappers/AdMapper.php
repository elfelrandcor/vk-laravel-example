<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace App\Vk\Mappers;


use App\Vk\Ad;

/**
 * Маппер данных для объявления в человекопонятный вид
 *
 * Class AdMapper
 * @package App\Vk\Mappers
 */
class AdMapper {

	protected $campaign;

	public static $labels = [
		'id' => 'id',
		'cpm' => 'Цена за 1000 показов',
		'day_limit' => 'Дневной лимит объявления в рублях',
		'all_limit' => 'Общий лимит объявления в рублях',
		'status' => 'Статус объявления',
		'name' => 'Название объявления',
		'impressions_limit' => 'Ограничение количества показов на одного пользователя',
		'cost_type' => 'Тип оплаты',
		'ad_format' => 'Формат объявления',
		'ad_platform' => 'Рекламные площадки',
		'approved' => 'Статус модерации объявления',
	];

	public function map($data): Ad {
		$result = [
			'Название кампании' => $this->campaign['name'],
		];
		foreach (self::$labels as $key => $name) {
			$result[$name] = $this->getValue($key, $data[$key]);
		}

		return new Ad($result);
	}

	protected function getValue($name, $data) {
		$method = sprintf('value_%s', $name);
		if (!method_exists($this, $method)) {
			return $data;
		}

		return $this->{$method}($data);
	}

	public function setCampaign($campaign) {
		$this->campaign = $campaign;

		return $this;
	}

	protected function value_cpm($data) {
		return $data / 100;
	}

	protected function value_status($data) {
		if (!$data) {
			return 'Объявление остановлено';
		}
		if ($data == 1) {
			return 'Объявление запущено';
		}
		if ($data == 2) {
			return 'Объявление удалено';
		}
		return 'Неверный статус';
	}

	protected function value_cost_type($data) {
		return $data ? 'Оплата за показы' : 'Оплата за переходы';
	}

	protected function value_ad_format($data) {
		$formats = [
			1 => 'изображение и текст',
			2 => 'большое изображение',
			3 => 'эксклюзивный формат',
			4 => 'продвижение сообществ или приложений, квадратное изображение',
			5 => 'приложение в новостной ленте (устаревший)',
			6 => 'мобильное приложение',
			9 => 'запись в сообществе',
			11 => 'адаптивный формат',
		];

		return $formats[$data];
	}

	protected function value_ad_platform($data) {
		$map = [
			'ВКонтакте и сайты-партнёры',
			'только ВКонтакте',
			'all' => 'все площадки',
			'desktop' => 'полная версия сайта',
			'mobile' => 'мобильный сайт и приложения',
		];

		return $map[$data];
	}

	protected function value_approved($data) {
		$map = [
			'объявление не проходило модерацию',
			'объявление ожидает модерации',
			'объявление одобрено',
			'объявление отклонено',
		];
		return $map[$data];
	}
}