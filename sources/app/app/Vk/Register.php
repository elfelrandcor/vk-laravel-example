<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace App\Vk;


use App\User;
use ATehnix\VkClient\Auth;
use ATehnix\VkClient\Client;

/**
 * Сервис регистрации пользователя
 *
 * Class Register
 * @package App\Vk
 */
class Register {

	protected $auth, $client;

	public function __construct(Auth $auth, Client $client) {
		$this->auth = $auth;
		$this->client = $client;
	}

	/**
	 * @param string $code
	 * @return $this
	 * @throws \ATehnix\VkClient\Exceptions\VkException
	 */
	public function createUser(string $code) {
		$data = $this->auth->getUserData($code);
		$token = Token::create(['value' => $data['access_token'], 'user_id' => $data['user_id']]);

		$response = $this
			->client
			->setDefaultToken($data['access_token'])
			->request('users.get', ['fields' => ['photo_50']])
		;
		$data = $response['response'][0];

		$name = sprintf('%s %s', $data['first_name'], $data['last_name']);
		/** @var User $user */
		$user = User::query()->firstOrNew(['name' => $name]);
		$user->picture = $data['photo_50'];
		$user->token_id = $token->id;

		$user->save();

		return $this;
	}
}