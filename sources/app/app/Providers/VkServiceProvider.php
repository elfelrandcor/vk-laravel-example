<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace App\Providers;


use App\Vk\Ads;
use App\Vk\Mappers\AdMapper;
use App\Vk\Token;
use ATehnix\VkClient\Auth;
use ATehnix\VkClient\Client;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class VkServiceProvider extends ServiceProvider {

	public function register() {
		$this->app->singleton(Client::class, function () {
			return new Client(config('vk-requester.version', '5.53'));
		});

		$this->app->singleton(Auth::class, function () {
			return new Auth(
				config('services.vkontakte.client_id'),
				config('services.vkontakte.client_secret'),
				config('services.vkontakte.redirect'),
				implode(',', config('vk-requester.scope', []))
			);
		});

		$this->app->singleton(Ads::class, function(Application $app) {
			return new Ads(
				$app->get(Client::class),
				Token::query()->first(),
				['ads' => new AdMapper()]
			);
		});
	}
}