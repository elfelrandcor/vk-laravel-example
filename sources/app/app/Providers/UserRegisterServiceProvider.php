<?php
/**
 * @author Juriy Panasevich <juriy.panasevich@gmail.com>
 */

namespace App\Providers;


use App\Vk\Register;
use ATehnix\VkClient\Auth;
use ATehnix\VkClient\Client;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class UserRegisterServiceProvider extends ServiceProvider {

	public function register()
	{
		$this->app->singleton(Register::class, function (Application $app) {
			return new Register($app->get(Auth::class), $app->get(Client::class));
		});
	}
}