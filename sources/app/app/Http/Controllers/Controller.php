<?php

namespace App\Http\Controllers;

use App\User;
use App\Vk\Ads;
use App\Vk\Note;
use App\Vk\Register;
use App\Vk\Token;
use ATehnix\VkClient\Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\URL;

class Controller extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


	public function index() {
		/** @var Token $token */
		if (!$token = Token::query()->first()) {
			return new RedirectResponse('vkauth');
		}
		\Illuminate\Support\Facades\Auth::login($token->user);

		return view('welcome');
	}

	public function token(Request $request, Auth $auth, Register $register) {
		if (Token::query()->first()) {
			return new RedirectResponse('/');
		}

		if ($request->exists('code')) {
			$register->createUser($request->get('code'));

			return new RedirectResponse('/');
		}

		return view('token', ['url' => $auth->getUrl()]);
	}

	public function accounts(Ads $ads) {
		return view('accounts', ['accounts' => $ads->getAccounts()]);
	}

	public function account(Request $request, Ads $ads) {
		return view('account', [
			'account' => $ads->getAccount($request->get('id')),
			'campaigns' => $ads->getCampaigns($request->get('id')),
		]);
	}

	public function campaign(Request $request, Ads $ads) {
		return view('campaign', [
			'account' => $ads->getAccount($request->get('account_id')),
			'campaign' => $ads->getCampaign($request->get('account_id'), $request->get('campaign_id')),
			'ads' => $ads->getAds($request->get('account_id'), $request->get('campaign_id')),
		]);
	}

	public function delete(Request $request, Ads $ads) {
		$ads->delete($request->get('account_id'), $request->get('id'));

		return new RedirectResponse(url()->previous());
	}

	public function note(Request $request) {
		$request->validate([
			'text' => 'max:100',
		]);

		$note = Note::query()->where(['ad_id' => $request->get('id')])->firstOrCreate(['ad_id' => $request->get('id')]);
		$note->text = $request->get('text');
		$note->save();

		return new RedirectResponse(url()->previous());
	}
}
