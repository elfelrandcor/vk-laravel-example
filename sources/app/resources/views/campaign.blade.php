@extends('layouts.app')

@section('content')

    <div>
        <strong>{{ $campaign['name'] }}</strong>
    </div>
    <div><a href="{{ route('account', ['id' => $account['account_id']]) }}">К списку кампаний</a></div>
    <div>
        <div><strong>Объявдения:</strong><hr></div>
            @foreach ($ads as $ad)
                <div>
                    <form action="{{ route('delete') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="account_id" value="{{ $account['account_id'] }}">
                        <input type="hidden" name="id" value="{{ $ad->id }}">
                        <input type="submit" value="Удалить" class="btn btn-danger" />
                    </form>
                    @foreach($ad->properties() as $name)
                        <p><strong>{{$name}}</strong>: {{ $ad->{$name} }}</p>
                    @endforeach

                    <strong>Заметка:</strong>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ route('note') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{ $ad->id }}">
                        <textarea name="text">{{ $ad->getNote() ? $ad->getNote()->text : '' }}</textarea>
                        <input type="submit" value="Сохранить" class="btn btn-success" />
                    </form>
                    <hr>
                </div>
            @endforeach
    </div>

@endsection