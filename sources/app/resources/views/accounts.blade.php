@extends('layouts.app')

@section('content')

    <strong>Список кабинетов:</strong>
    <ul>
        @foreach ($accounts as $account)
            <li><a href="{{ route('account', ['id' => $account['account_id']]) }}">{{ $account['account_name'] }}</a></li>
        @endforeach
    </ul>

@endsection