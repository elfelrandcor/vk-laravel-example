@extends('layouts.app')

@section('content')

    <div>
        <strong>{{ $account['account_name'] }}</strong>
    </div>
    <div><a href="{{ route('accounts') }}">К списку кабинетов</a></div>

    <div>
        <strong>Кампании:</strong>
        <ul>
        @foreach ($campaigns as $campaign)
            <li>
                <a href="{{ route('campaign', ['account_id' => $account['account_id'], 'campaign_id' => $campaign['id']]) }}">{{ $campaign['name'] }}</a>
            </li>
        @endforeach
        </ul>
    </div>

@endsection