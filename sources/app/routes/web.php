<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index');
Route::get('vkauth', 'Controller@token');
Route::get('accounts', 'Controller@accounts')->name('accounts');
Route::get('account', 'Controller@account')->name('account');
Route::get('campaign', 'Controller@campaign')->name('campaign');
Route::post('delete', 'Controller@delete')->name('delete');
Route::post('note', 'Controller@note')->name('note');

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');